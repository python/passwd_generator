# -*- coding: utf-8 -*-

import datetime
import random
from pathlib import Path

def ExitOnError():
	""" Simply dysplays a message, then quit """
	print ("The program will now exit. Press Enter to continue...")
	input()
	sys.exit(0)

def CheckFile(file):
	""" Check if the speciefied file really exists in the current working directory """
	characterFile = Path(file)
	if characterFile.is_file() == False:
		print ("Fatal error : File characters.txt not found ! ")
		ExitOnError()

def LoadCharacters(file):
	""" Return a string which contains the characters loaded from the file 'character.txt' """
	fileReader = open(file, "r")
	string = ""
	for char in fileReader:
		string += char
	if string == "" :
		print ("Fatal error : There isn't any character to load !")
		ExitOnError()
	else : 
		return string
		
def CheckUserInput():
	""" Verify if the user's input is matching expectations, then return an integer """
	userInput = 0
	while True:
		try:
			userInput = int(input("Desired length (2-20) : "))
		except ValueError:
			print("Error : Not an Integer !")
			print("-----------------------")
			# go back to loop beginning
		else:
			if userInput >= 2 and userInput <= 20:
				return userInput
			else:
				print("Error : Incorrect length !")
				print("-----------------------")
				#go back to loop beginning
				
def WriteDateIntoFile(file):
	""" Writes the current time and date in french format into the output file """
	formattedDate = str(datetime.datetime.now().strftime('%d/%m/%Y-%Hh%M'))
	with open(file, 'a') as dateWriter:
		dateWriter.write("\n===" + formattedDate + "===" + "\n")
		
def BuildPassword(passwordLength, data):
	""" Builds a password from 'data' using the length chosen by the user """
	output = ""
	for x in range (0, passwordLength):
		output += str(random.choice(data))
	return output 
	
def WritePasswordIntoFile(file, text):
	""" Writes the built password into output file """
	with open(file, 'a') as textWriter:
		textWriter.write("\n" + str(chosenLength) + " --> " + text + "\n")
	
# Program start-up
CheckFile("characters.txt")
characters = LoadCharacters("characters.txt")
WriteDateIntoFile("text.txt")
print ("Possible characters :\n" + characters)
print ("----------------------")

# Program core
while True:
	chosenLength = CheckUserInput()
	password = BuildPassword(chosenLength, characters)
	WritePasswordIntoFile("text.txt", password)
	print ("Password : " + password)
	print ("----------------------")
	# go back to loop beginning
